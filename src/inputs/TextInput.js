function TextInput({ type, label, name, placeholder }) {
  return (
    <div>
      <label>{label}</label>
      <input type={type} placeholder={placeholder} name={name} />
    </div>
  );
}

export default TextInput;
