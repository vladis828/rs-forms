function RadioInput({ options, name }) {
  return (
    options &&
    options.map((option) => (
      <div key={option}>
        <input type="radio" id={option} name={name} value={option} />
        <label htmlFor="html">{option}</label>
      </div>
    ))
  );
}

export default RadioInput;
