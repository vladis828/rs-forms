function WithIcon({ label, placeholder, name }) {
  return (
    <div className="input-wrapper">
      <label>{label}</label>
      <span className="leading-symbol">@</span>
      <input type="text" placeholder={placeholder} name={name} />
    </div>
  );
}

export default WithIcon;
