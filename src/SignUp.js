import { useState, useRef } from "react";
import TextInput from "./inputs/TextInput";
import WithIcon from "./inputs/WithIcon";
import RadioInput from "./inputs/RadioInput";

const SignUp = ({ onSubmit }) => {
  const [inputForm, setInputForm] = useState({});
  const formRef = useRef(null);
  const onChange = (e) => {
    setInputForm((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const submitHandler = (e) => {
    e.preventDefault();
    onSubmit(inputForm);
    formRef.current.reset();
  };

  const handleReset = () => {
    setInputForm({});
  };

  return (
    <>
      <form
        onSubmit={submitHandler}
        className="form"
        onChange={(e) => onChange(e)}
        ref={formRef}
        onReset={handleReset}
      >
        <TextInput
          type="text"
          label="Name:"
          name="name"
          placeholder="Enter name"
          className="input-field"
        />
        <WithIcon
          label="Nickname:"
          placeholder="Enter nickname"
          name="nickname"
        />
        <TextInput
          type="email"
          label="Email:"
          placeholder="Enter email"
          name="email"
        />
        <RadioInput options={["Male", "Female"]} name="gender" />
        <TextInput
          type="password"
          label="Password:"
          placeholder="Enter password"
          name="password"
        />
        <TextInput
          type="password"
          label="Repeat password:"
          placeholder="Repeat password"
          name="repeat password"
        />
        <input type="submit" value="Sign Up" className="clickable"></input>
      </form>
    </>
  );
};

export default SignUp;
