import { useState, useRef } from "react";
import TextInput from "./inputs/TextInput";

const SignIn = ({ onSubmit, signUpHandler }) => {
  const [inputForm, setInputForm] = useState({});
  const formRef = useRef(null);
  const onChange = (e) => {
    setInputForm((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const submitHandler = (e) => {
    e.preventDefault();
    onSubmit(inputForm);
    formRef.current.reset();
  };

  const handleReset = () => {
    setInputForm({});
  };

  return (
    <>
      <form
        onSubmit={submitHandler}
        className="form"
        onChange={(e) => onChange(e)}
        ref={formRef}
        onReset={handleReset}
      >
        <TextInput
          type="email"
          label="Email:"
          name="email"
          placeholder="Enter email"
        />
        <TextInput
          type="password"
          label="Password:"
          name="password"
          placeholder="Enter password"
        />
        <input type="submit" value="Sign In" className="clickable"></input>
      </form>
      <div className="clickable">
        <h5 onClick={signUpHandler}>Sign Up</h5>
      </div>
    </>
  );
};

export default SignIn;
