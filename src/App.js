import { useState } from "react";
import SignIn from "./SignIn.js";
import SignUp from "./SignUp.js";
import "./App.css";

function App() {
  const [showSignUp, setShowSignUp] = useState(false);

  const signUpHandler = () => {
    setShowSignUp(true);
  };

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <div className="App">
      <header className="App-header">
        {showSignUp ? (
          <SignUp onSubmit={onSubmit} />
        ) : (
          <SignIn onSubmit={onSubmit} signUpHandler={signUpHandler} />
        )}
      </header>
    </div>
  );
}

export default App;
